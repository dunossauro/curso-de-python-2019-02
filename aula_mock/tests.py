from unittest import TestCase
from unittest import mock
import app


class TestApp(TestCase):
    def test_parse_files_deve_existir(self):
        self.assertEqual(
            hasattr(app, 'parse_files'),
            True,
            'parse_files não foi definido'
        )

    def test_parse_files_deve_ser_chamavel(self):
        self.assertEqual(
            hasattr(app.parse_files, '__call__'),
            True,
            'parse_files não é chamavel'
        )

    def test_parse_files_deve_receber_dois_argumentos(self):
        self.assertEqual(
            app.parse_files.__code__.co_argcount,
            2,
            'parse_files não recebe dois parametros'
            )

    def test_parse_file_deve_abrir_o_arquivo_de_entrada_primeiro(self):
        with mock.patch('app.open') as spy:
            app.parse_files('a', 'b')

        self.assertEqual(
            mock.call('a'),
            spy.call_args_list[0]
        )

    def test_parse_file_deve_abrir_o_arquivo_de_saida_em_modo_de_escrita(self):
        with mock.patch('app.open') as spy:
            app.parse_files('a', 'b')

        spy.assert_called_with('b', 'w')


    def test_parse_file_deve_ver_o_conteudo_do_arquivo_de_entrada(self):
        fake_file = mock.MagicMock()
        with mock.patch('app.open', return_value=fake_file) as mocked_open:
            app.parse_files('a', 'b')

        fake_file.read.assert_called()

    def test_parse_file_deve_fechar_o_arquivo_de_entrada(self):
        fake_file = mock.MagicMock()
        with mock.patch('app.open', return_value=fake_file) as mocked_open:
            app.parse_files('a', 'b')

        fake_file.close.assert_called()

    def test_parse_file_deve_escrever_o_conteudo_do_arquivo_de_saida(self):
        fake_file_in = mock.MagicMock()
        fake_file_out = mock.MagicMock()
        fake_file_in.read.return_value = '1 2 3'
        numero_de_chamadas_espedaradas = len('1 2 3'.split())

        with mock.patch(
            'app.open', side_effect=[fake_file_in, fake_file_out]
        ) as mocked_open:
            app.parse_files('a', 'b')
        self.assertEqual(
            numero_de_chamadas_espedaradas,
            len(fake_file_out.method_calls)
        )

    def test_parse_file_deve_escrever_o_conteudo_do_arquivo_de_saida_com_quebra_de_linha(self):
        fake_file_in = mock.MagicMock()
        fake_file_out = mock.MagicMock()
        fake_file_in.read.return_value = '1'

        with mock.patch(
            'app.open', side_effect=[fake_file_in, fake_file_out]
        ) as mocked_open:
            app.parse_files('a', 'b')

        fake_file_out.write.assert_called_with('1\n')
