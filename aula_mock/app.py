def parse_files(file_in, file_out):
    opened_in = open(file_in)
    content = opened_in.read()
    opened_in.close()

    # --------------------------

    opened_out = open(file_out, 'w')
    for palavra in content.split():
        opened_out.write(palavra + '\n')
