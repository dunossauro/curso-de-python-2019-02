import unittest
import problema
from unittest import mock


class TestProblema(unittest.TestCase):
    def test_verifica_se_função_de_soma_existe(self):
        self.assertEqual(
            hasattr(problema, 'soma'),
            True,
            'Soma não existe'
        )

    def test_verifica_se_soma_tem_paramêtros(self):
        self.assertEqual(
            problema.soma.__code__.co_argcount,
            2,
            'soma não recebe dois parametros'
        )

    def test_verifica_se_soma_2_2_deve_retornar_4(self):
        self.assertEqual(
            problema.soma(2, 2),
            4
        )

    def test_verifica_se_soma_5_5_deve_retornar_10(self):
        self.assertEqual(
            problema.soma(5, 5),
            10
        )

    def test_soma_primeiro_valor_deve_chamar_método_de_soma(self):
        fake_number = mock.MagicMock()
        problema.soma(fake_number, 10)
        fake_number.__add__.assert_called()

    def test_verifica_se_função_de_sub_existe(self):
        self.assertEqual(
            hasattr(problema, 'sub'),
            True,
            'Sub não existe'
        )

    def test_verifica_se_função_de_sub_é_invocável(self):
        self.assertEqual(
            hasattr(problema.sub, '__call__'),
            True,
            'Sub não é invocável'
        )

    def test_verifica_se_função_sub_tem_dois_parametros(self):
        self.assertEqual(
            problema.sub.__code__.co_argcount,
            2,
            'soma não recebe dois parametros'
        )

    def test_sub_primeiro_valor_deve_chamar_método_de_sub(self):
        fake_number = mock.MagicMock()
        problema.sub(fake_number, 10)
        fake_number.__sub__.assert_called()

    def test_sub_de_10_5_deve_retornar_5(self):
        self.assertEqual(
            problema.sub(10, 5),
            5
        )

    def test_verifica_se_exp_existe(self):
        self.assertEqual(
            hasattr(problema, 'exp'),
            True,
            'exp não existe'
        )

    def test_verifica_se_exp_é_invocável(self):
        self.assertEqual(
            hasattr(problema.exp, '__call__'),
            True
        )

    def test_verifica_se_exp_recebe_3_parametros(self):
        self.assertEqual(
            problema.exp.__code__.co_argcount,
            3,
        )

    @mock.patch('problema.sub')
    def test_verificar_se_exp_invoca_soma(self, mocked_sub):
        with mock.patch('problema.soma') as spy:
            problema.exp('Dummy', 'Dummy', 'Dummy')

        spy.assert_called_with('Dummy', 'Dummy')

    def test_verificar_se_exp_invoca_sub(self):
        with mock.patch('problema.sub') as spy:
            problema.exp('Dummy', 'Dummy', 'Dummy')

        spy.assert_called_with('Dummy', 'Dummy')
